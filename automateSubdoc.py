# Author: Anderson Lebbad
# Date: 12/24/2015

# This script is designed to search through an organized directory structure of latex files and create versions of the files that can be input to a main file.

# Unix style pathname pattern expansion library
import glob

# Get the list of categories
categories = glob.glob('./**/')

for category in categories:
	category_title = category[2:-1]
	print(category_title)
	recipes = glob.glob(category + '**/')
	for recipe_dir in recipes:
		recipe = recipe_dir[len(category):-1]
		print('\t' + recipe)
		read_path = recipe_dir + '\\' + recipe + '.tex'
		# Open the standalone document and read its contents
		with open(read_path, 'r') as f:
			read_data = f.readlines()
		# Find the title line
		title_lines = [s for s in read_data if "\\title{" in s]
		# Extract the title line
		title_line = title_lines[0];
		# Extract the contents of the title line
		title_contents = title_line[7:-2]
		# Find the author
		author_lines = [s for s in read_data if "\\author{" in s]
		# Extract the author line
		author_line = author_lines[0]
		# Extract the contents of the author line
		author_contents = author_line[8:-2]
		# Create line for recipe heading
		recipe_heading = title_contents + r' \\ \Large ' + author_contents
		# Separate title from details. Find the first line return
		return_idx = recipe_heading.find('\\\\')
		recipe_title = recipe_heading[:return_idx-1]
		recipe_details = recipe_heading[return_idx+3:]

		# Find the begin document line
		begin_idx = read_data.index('\\begin{document}\n')
		# Find the end document line
		end_idx = read_data.index('\\end{document}\n')
		# Create subdocument list
		sub_doc = read_data[begin_idx:end_idx]
		# Find maketitle line
		title_line = sub_doc.index('\\maketitle\n')

		# Replace the begin document with the subsection line containing the recipe title
		sub_doc[0] = '\\chapter*{' + recipe_title + '}\n'
		# Insert line to add un-numbered chapter to ToC
		sub_doc.insert(1,'\\addcontentsline{toc}{chapter}{' + recipe_title + '}\n')

		# Declare the write path
		recipe_write_path = recipe_dir + '\\INPUT_' + recipe + '.tex'
		# Open the document to be written to, and write content
		with open(recipe_write_path, 'w') as f:
			f.writelines(sub_doc)

	# Once the individual recipe files are set to input for the category, create the category file
	category_write_path = category + category_title + '.tex'
	with open(category_write_path, 'w') as f:
		# Find the input files
		input_files = glob.glob(category + '**\\INPUT_*.tex')
		# Removes the higher level directory, for referencing
		input_files = [w.replace('.\\','') for w in input_files]
		# Remove the .tex appendix
		input_files = [w.replace('.tex','') for w in input_files]
		# Change all \ to /
		input_files = [w.replace('\\','/') for w in input_files]
		# Replace underscores with spaces
		category_title = category_title.replace('_',' ')
		# Section opening lines
		section_lines = ['\\part{' + category_title + '}\n', '\n']
		for input_file in input_files:
			section_lines.append('\\input{' + input_file + '}\n')
			section_lines.append('\n')
		f.writelines(section_lines)


